#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>

#ifndef ALGORYTM_H
#define ALGORYTM_H

bool myfn(std::pair<int, int> i, std::pair<int, int> j);

struct zadanie{
	int czasNominalny;
	int czasNajmniejszy;
	int czasRzeczywisty;
	int kosztSkrocenia;
	zadanie() :czasNajmniejszy(0), czasNominalny(0), kosztSkrocenia(0), czasRzeczywisty(0){}
};

class algorytm{
	
	int N,M,H;
	int czasProjektu;
	std::vector<zadanie> listaZadan;
	std::vector<std::vector<int>> macierzSasiedztwa;
	std::vector<int> sciezkaKrytyczna;
public:
	bool WczytywanieDanych(std::string nazwaPliku);
	void WyswietlMacierzSasiedztwa();
	int WyznaczCzasTrwania();
	void wyznaczSciezkaKrytyczna(const int koniec, std::vector<int> tabPoprzednikow);
	std::vector<int> getSciezkaKrytyczna();
	void Obniz(int i);
	void Obnizv2(int i);
	void ObnizLosowo();
	void Wyczysc();
};



#endif