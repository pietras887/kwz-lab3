30 90 15000
93 37 27  84 59 29  53 24 98  42 9 98  42 9 22  68 32 80  70 18 82  52 33 62  4 4 73  65 13 2  1 1 68  67 14 83  5 2 51  75 65 89  6 2 42  53 37 68  59 57 94  22 5 45  38 4 9  68 6 39  7 6 63  10 5 28  68 60 44  93 34 21  14 3 91  46 26 14  83 1 1  53 42 78  38 38 16  1 1 83
2 10  3 5  3 12  3 13  3 15  3 23  3 27  3 29  3 30  4 7  5 10  5 20  6 9  6 10  6 27  6 28  8 16  9 1  9 2  9 4  9 16  9 17  9 20  11 2  11 5  11 10  11 17  11 20  11 27  12 22  13 8  13 21  13 24  14 3  14 4  14 6  14 21  14 23  14 29  15 7  15 12  15 16  15 17  15 20  15 22  16 10  16 12  16 22  17 1  17 4  17 10  18 6  18 24  19 2  19 4  19 17  19 22  19 28  20 2  20 17  20 22  21 22  22 7  23 4  23 6  23 7  23 15  23 28  24 16  24 17  24 19  25 4  25 9  25 10  25 23  26 2  26 18  26 20  26 21  26 22  26 27  27 4  27 7  27 28  29 7  29 9  29 16  30 4  30 22  30 28

in:
  - Pierwsza linia zawiera trzy liczby.
    N - liczba zadan, M - licza polaczen, B - dostepny budzet.
  - Druga linia zawiera N trojek H,L,C, opisujace zadania.
    H - nominalny czasu wykonywania zadania, L - najmniejszy
    mozliwy czas, C - koszt jednostkowy za skrocenie zadania.
  - Trzecia linia zawiera M zaleznosci kolejnosciowych
out:
  - Czas projektu bez redukcji.
  - Czas projektu po redukcji.
  - Calkowity koszt redukcji czasu.
  - N wartosci redukcji w kolejnych zadaniach

no reduction scheduler time:
507
reduction scheduler time:
327
reduction cost:
14967
reduction:
34 0 10 0 0 36 52 0 0 33 0 5 0 10 0 16 2 0 32 62 0 5 8 3 0 0 17 0 0 0

