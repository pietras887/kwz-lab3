#include "algorytm.h"
using namespace std;
bool algorytm::WczytywanieDanych(string nazwaPliku)
{
	std::ifstream plik(nazwaPliku);
	if (!plik.is_open())
	{
		std::cerr << "Jest kiepsko :(" << std::endl;
		return false;
	}

	zadanie z;
	plik >> N >> M >> H;
	for (int i = 0; i < N; i++){
		plik >> z.czasNominalny >> z.czasNajmniejszy >> z.kosztSkrocenia;
		z.czasRzeczywisty = z.czasNominalny;
		listaZadan.push_back(z);
	}

	macierzSasiedztwa = vector<vector<int>>(N, vector<int>(N, false));
	int from, to;
	for (int i = 0; i < M; i++)
	{
		plik >> from >> to;
		macierzSasiedztwa[from - 1][to - 1] = true;
	}
	return true;
}

void algorytm::WyswietlMacierzSasiedztwa()
{
	for (int x = 0; x < macierzSasiedztwa.size(); x++){
		for (int y = 0; y < macierzSasiedztwa.size(); y++)
			cout << macierzSasiedztwa[x][y] << " ";
		cout << endl;
	}
}

int algorytm::WyznaczCzasTrwania()
{
	int v = 0;
	int y_max = 0;
	std::vector<int> d(N, listaZadan[0].czasRzeczywisty);
	std::vector<int> p(N, -1);
	bool test = false;
	for (int i = 2; i <= N && !test; i++)
	{
		test = true;
		for (int x = 0; x < N; x++)
			for (int y = 0; y < N; y++)
				if (macierzSasiedztwa[x][y] == 1 && d[y] < d[x] + listaZadan[y].czasRzeczywisty)
				{
					test = false;
					d[y] = d[x] + listaZadan[y].czasRzeczywisty;
					p[y] = x;
					czasProjektu = std::max(d[y], czasProjektu);
					if (czasProjektu == d[y])
						y_max = y;
				}
	}
	int czasTrwania = 0;
	wyznaczSciezkaKrytyczna(y_max, p);
	auto sciezka = getSciezkaKrytyczna();
	for (auto i : sciezka)
		czasTrwania += listaZadan[i-1].czasRzeczywisty;
	return czasTrwania;
}
void algorytm::wyznaczSciezkaKrytyczna(const int koniec, std::vector<int> tabPoprzednikow)
{
	int index = koniec;
	sciezkaKrytyczna = std::vector<int>();

	while (tabPoprzednikow[index] != -1)
	{
		sciezkaKrytyczna.push_back(index + 1);
		index = tabPoprzednikow[index];
	}

	sciezkaKrytyczna.push_back(index + 1);
	std::reverse(sciezkaKrytyczna.begin(), sciezkaKrytyczna.end());
}

std::vector<int> algorytm::getSciezkaKrytyczna()
{
	return sciezkaKrytyczna;
}

void algorytm::Obniz(int ktory)
{
	string nazwa_pliku = "output" + std::to_string(ktory) + ".txt";
	std::ofstream plik(nazwa_pliku);
	auto sciezka = getSciezkaKrytyczna();
	czasProjektu = WyznaczCzasTrwania();
	int oldCzasProjektu = czasProjektu;
	int newCzasProjektu = 0;
	std::vector<zadanie> newListaZadan = listaZadan;
	int newBudzet = H;
	int oldBudzet = H;
	bool koniec = false;
	while (!koniec){
		koniec = true;
		WyznaczCzasTrwania();
		sciezka = getSciezkaKrytyczna();
		for (auto i : sciezka){
			if ((newListaZadan[i - 1].czasNominalny - newListaZadan[i - 1].czasNajmniejszy) > 0 && (newListaZadan[i - 1].kosztSkrocenia * (newListaZadan[i - 1].czasNominalny - newListaZadan[i - 1].czasNajmniejszy)) <= newBudzet){
				newBudzet -= newListaZadan[i - 1].kosztSkrocenia * (newListaZadan[i - 1].czasNominalny - newListaZadan[i - 1].czasNajmniejszy);
				newListaZadan[i - 1].czasRzeczywisty = newListaZadan[i - 1].czasNajmniejszy;
			}
			newCzasProjektu += newListaZadan[i - 1].czasRzeczywisty;
		}
		if (newBudzet < 0)
			koniec = false;
	}

	czasProjektu = newCzasProjektu;
	listaZadan = newListaZadan;
	H = newBudzet;

	plik << N << " " << M << " " << H << " " << endl;
	cout << "Czas projektu przed skroceniem: " << oldCzasProjektu << endl;
	plik << "Czas projektu przed skroceniem: " << oldCzasProjektu << endl;
	cout << "Czas Projektu po skroceniu" << WyznaczCzasTrwania() << endl;
	plik << "Czas Projektu po skroceniu" << WyznaczCzasTrwania() << endl;
	sciezka = getSciezkaKrytyczna();
	cout << "Budzet " << oldBudzet << " Budzet zmniejszony " << newBudzet << endl;
	plik << "Budzet " << oldBudzet << " Budzet zmniejszony " << newBudzet << endl;
	if (oldCzasProjektu > czasProjektu)
		cout << "Not bad! - czas skrocony" << endl;
	else
		cout << "Fail! - czas nieskrocony" << endl;
}


void algorytm::ObnizLosowo()
{
	int oldCzasProjektu = WyznaczCzasTrwania();
	int newCzasProjektu = 0;
	std::vector<zadanie> newListaZadan = listaZadan;
	std::vector<bool> sprawdzone(listaZadan.size(), false);
	int spr = 0;
	int newBudzet = H;
	int oldBudzet = H;
	srand(time(NULL));
	while (H > 0 && spr != sprawdzone.size())
	{
		int i = rand() % listaZadan.size();
		if (sprawdzone[i] != true)
		{
			sprawdzone[i] = true;
			spr++;
		}

		while (newBudzet - listaZadan[i].kosztSkrocenia > 0 && newListaZadan[i].czasRzeczywisty > listaZadan[i].czasNajmniejszy){
			newListaZadan[i].czasRzeczywisty--;
			newBudzet -= listaZadan[i].kosztSkrocenia;
		}

	}
	czasProjektu = newCzasProjektu;
	listaZadan = newListaZadan;
	H = newBudzet;

	cout << "Czas projektu przed skroceniem: " << oldCzasProjektu << endl;
	cout << "Czas projektu po skroceniu: " << WyznaczCzasTrwania() << endl;
	cout << "Budzet " << oldBudzet << " Budzet zmniejszony " << newBudzet << endl;
	if (oldCzasProjektu > czasProjektu)
		cout << "Not bad! - czas skrocony" << endl;
	else
		cout << "Fail! - czas nieskrocony" << endl;
}

void algorytm::Obnizv2(int ktory)
{
	string nazwa_pliku = "outputv2" + std::to_string(ktory) + ".txt";
	std::ofstream plik(nazwa_pliku);
	auto sciezka = getSciezkaKrytyczna();
	czasProjektu = WyznaczCzasTrwania();
	int oldCzasProjektu = czasProjektu;
	int newCzasProjektu = 0;
	std::vector<zadanie> newListaZadan = listaZadan;
	int newBudzet = H;
	int oldBudzet = H;
	std::vector<std::pair<int, int>> czasy;
	int tmp=0,wierzcholek;
	bool koniec = false;
	while (!koniec){
		koniec = true;
		WyznaczCzasTrwania();
		sciezka = getSciezkaKrytyczna();
		for (auto i : sciezka){
			czasy.push_back(std::pair<int, int>(i, newListaZadan[i - 1].czasRzeczywisty));
		}
		std::sort(czasy.begin(), czasy.end(), myfn);
		int i = 0;
		wierzcholek = czasy[i].first - 1;
		while (newListaZadan[wierzcholek].czasRzeczywisty == newListaZadan[wierzcholek].czasNajmniejszy && i<czasy.size())
			wierzcholek = czasy[i++].first - 1;
		if (newBudzet>newListaZadan[wierzcholek].kosztSkrocenia && newListaZadan[wierzcholek].czasRzeczywisty > newListaZadan[wierzcholek].czasNajmniejszy){
			newListaZadan[wierzcholek].czasRzeczywisty--;
			newBudzet -= newListaZadan[wierzcholek].kosztSkrocenia;
			koniec = false;
		}
	}
		czasProjektu = newCzasProjektu;
		listaZadan = newListaZadan;
		H = newBudzet;
		czasy.clear();
	

		plik << N << " " << M << " " << H << " " << endl;
		cout << "Czas projektu przed skroceniem: " << oldCzasProjektu << endl;
		plik << "Czas projektu przed skroceniem: " << oldCzasProjektu << endl;
		cout << "Czas Projektu po skroceniu" << WyznaczCzasTrwania() << endl;
		plik << "Czas Projektu po skroceniu" << WyznaczCzasTrwania() << endl;
		sciezka = getSciezkaKrytyczna();
		cout << "Budzet " << oldBudzet << " Budzet zmniejszony " << newBudzet << endl;
		plik << "Budzet " << oldBudzet << " Budzet zmniejszony " << newBudzet << endl;
	if (oldCzasProjektu > czasProjektu)
		cout << "Not bad! - czas skrocony" << endl;
	else
		cout << "Fail! - czas nieskrocony" << endl;
}
void algorytm::Wyczysc()
{
	N = 0; N = 0; H = 0;
	czasProjektu=0;
	std::vector<zadanie> listaZadan;
	listaZadan.clear();
	for (int i = 0; i<N; i++)
		macierzSasiedztwa[i].clear();
	macierzSasiedztwa.clear();
	sciezkaKrytyczna.clear();
}
bool myfn(std::pair<int, int> i, std::pair<int, int> j) { return i.second>j.second; }